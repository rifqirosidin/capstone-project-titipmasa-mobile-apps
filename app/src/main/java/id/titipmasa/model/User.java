package id.titipmasa.model;

public class User {

    String nama, email;
    String id_user, alamat, nik, noHP, urlImageProfil, urlImageKTP;

    public User(){

    }

    public User(String id_user, String nama, String email) {
        this.nama = nama;
        this.email = email;
        this.id_user = id_user;
    }

    public User(String id_user, String nama,  String alamat, String nik, String noHP, String urlImageProfil, String urlImageKTP) {
        this.id_user = id_user;
        this.nama = nama;

        this.alamat = alamat;
        this.nik = nik;
        this.noHP = noHP;
        this.urlImageProfil = urlImageProfil;
        this.urlImageKTP = urlImageKTP;
    }


    public String getId_user(){
        return id_user;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getNik() {
        return nik;
    }

    public String getNoHP() {
        return noHP;
    }

    public String getUrlImageProfil() {
        return urlImageProfil;
    }

    public String getUrlImageKTP() {
        return urlImageKTP;
    }





    public String getNama() {
        return nama;
    }

    public String getEmail() {
        return email;
    }


}
