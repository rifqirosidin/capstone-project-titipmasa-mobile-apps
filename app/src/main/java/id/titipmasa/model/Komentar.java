package id.titipmasa.model;

public class Komentar {

    String isi_komentar, user_id, imgProfil, id_traveller;
    String id, nama, imgPesan;
    String id_barang;

    public Komentar(){

    }
//    public Komentar(String id, String user_id, String nama,String isi_komentar, String imgProfil, String id_traveller, String imgPesan) {
//        this.id = id;
//        this.user_id = user_id;
//        this.nama = nama;
//        this.isi_komentar = isi_komentar;
//        this.user_id = user_id;
//        this.imgProfil = imgProfil;
//        this.id_traveller = id_traveller;
//        this.imgPesan = imgPesan;
//    }

    public Komentar(String id, String user_id, String id_barang, String nama,String isi_komentar, String imgProfil, String id_traveller, String imgPesan) {
        this.id = id;
        this.id_barang = id_barang;
        this.nama = nama;
        this.user_id = user_id;
        this.isi_komentar = isi_komentar;
        this.user_id = user_id;
        this.imgProfil = imgProfil;
        this.id_traveller = id_traveller;
        this.imgPesan = imgPesan;
    }

    public String getId_barang(){
        return id_barang;
    }

    public  String getId(){
        return id;
    }
    public String getImgPesan(){
        return imgPesan;
    }

    public String getNama(){
        return nama;
    }

    public String getIsi_komentar() {
        return isi_komentar;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getImgProfil() {
        return imgProfil;
    }

    public String getId_traveller() {
        return id_traveller;
    }
}
