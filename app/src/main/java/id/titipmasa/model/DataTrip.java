package id.titipmasa.model;

public class DataTrip {
    String nama;
    String pulang;
    String lokasi;
    String tujuan, id;
    String imgProfil;
    String user_id;

    public DataTrip(){

    }


    public DataTrip(String id, String user_id,String nama,String tujuan, String pulang, String lokasi, String imgProfil) {
        this.nama = nama;
        this.user_id = user_id;
        this.pulang = pulang;
        this.lokasi = lokasi;
        this.tujuan = tujuan;
        this.id = id;
        this.imgProfil = imgProfil;
    }


    public String getNama() {
        return nama;
    }

    public String getId() {
        return id;
    }

    public String getPulang() {
        return pulang;
    }

    public String getLokasi() {
        return lokasi;
    }

    public String getTujuan() {
        return tujuan;
    }

    public String getImgProfil(){
        return  imgProfil;
    }

    public String getUser_id(){
        return user_id;
    }

}
