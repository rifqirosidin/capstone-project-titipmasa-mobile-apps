package id.titipmasa.model;



public class Feedback {

    String isi_feedback, user_id, imgProfil, id_traveller;
    String id, nama, id_barang;

    public Feedback() {

    }

    public Feedback(String id, String user_id, String id_barang, String id_traveller, String nama, String imgProfil, String isi_feedback) {
        this.isi_feedback = isi_feedback;
        this.user_id = user_id;
        this.id_barang = id_barang;
        this.imgProfil = imgProfil;
        this.id_traveller = id_traveller;
        this.id = id;
        this.nama = nama;
    }

    private String getId_barang(){
        return  id_barang;
    }
    public String getIsi_feedback() {
        return isi_feedback;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getImgProfil() {
        return imgProfil;
    }

    public String getId_traveller() {
        return id_traveller;
    }

    public String getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }
}
