package id.titipmasa.model;

public class TitipBarang {

    String namaBarang, jumlahBarang, hargaBarang, tujuanPengiriman, imgUrl, id_titipan, user_id;
    String id_traveller;
    String nama_penitip, status;
    public TitipBarang(){

    }

    public TitipBarang(String id_titipan,  String user_id, String id_traveller, String nama_penitip, String namaBarang, String jumlahBarang, String hargaBarang, String tujuanPengiriman, String imgUrl, String status) {
        this.id_titipan = id_titipan;
        this.user_id = user_id;
        this.nama_penitip = nama_penitip;
        this.id_traveller = id_traveller;
        this.namaBarang = namaBarang;
        this.jumlahBarang = jumlahBarang;
        this.hargaBarang = hargaBarang;
        this.tujuanPengiriman = tujuanPengiriman;
        this.imgUrl = imgUrl;
        this.status = status;

    }

    public String getStatus(){
        return status;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public String getJumlahBarang() {
        return jumlahBarang;
    }

    public String getHargaBarang() {
        return hargaBarang;
    }

    public String getTujuanPengiriman() {
        return tujuanPengiriman;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getId_titipan() {
        return id_titipan;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getId_traveller(){
        return id_traveller;
    }

    public String getNama_penitip(){
        return nama_penitip;
    }


}
