package id.titipmasa.model;

public class RekomendasiBarang {

    String id, nama_barang, harga_barang, imgBarang, keterangan;
    String user_id;

    String id_traveller, imgProfilTraveller;

    public RekomendasiBarang(){

    }

    public RekomendasiBarang(String id, String user_id,String nama_barang, String harga_barang, String imgBarang, String keterangan) {
        this.id = id;
        this.user_id = user_id;
        this.nama_barang = nama_barang;
        this.harga_barang = harga_barang;
        this.imgBarang = imgBarang;
        this.keterangan = keterangan;
    }

    public String getId() {
        return id;
    }
    public String getUser_id(){
        return user_id;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public String getHarga_barang() {
        return harga_barang;
    }

    public String getImgBarang() {
        return imgBarang;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public String getId_traveller() {
        return id_traveller;
    }

    public void setId_traveller(String id_traveller) {
        this.id_traveller = id_traveller;
    }

    public String getImgProfilTraveller() {
        return imgProfilTraveller;
    }

    public void setImgProfilTraveller(String imgProfilTraveller) {
        this.imgProfilTraveller = imgProfilTraveller;
    }
}
