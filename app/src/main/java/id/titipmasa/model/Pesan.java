package id.titipmasa.model;

public class Pesan {

    String isi_pesan;
    String from_nama;
    String user_id;
    String id;
    String to_user_id;
    String to_name;
    String imgProfil;
    String imgProfilPenerima;
    String imgPesan;


    public Pesan(){

    }

    public Pesan(String id, String user_id, String isi_pesan, String from_nama,String to_name, String to_user_id, String imgProfil, String imgProfilPenerima, String imgPesan) {
        this.isi_pesan = isi_pesan;
        this.user_id = user_id;
        this.to_name = to_name;
        this.from_nama = from_nama;
        this.id = id;
        this.to_user_id = to_user_id;
        this.imgProfil = imgProfil;
        this.imgProfilPenerima = imgProfilPenerima;
        this.imgPesan = imgPesan;
    }

    public String getTo_name() {
        return to_name;
    }



    public String getImgProfilPenerima(){
        return imgProfilPenerima;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }


    public String getIsi_pesan() {
        return isi_pesan;
    }

    public String getFrom_nama() {
        return from_nama;
    }

    public String getId(){
        return id;
    }


    public String getUser_id() {
        return user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }
    public String getImgProfil() {
        return imgProfil;
    }

    public String getImgPesan(){
        return imgPesan;
    }

}
