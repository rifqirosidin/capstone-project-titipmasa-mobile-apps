package id.titipmasa;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.adapter.FeedbackAdapter;
import id.titipmasa.adapter.KomentarAdapter;
import id.titipmasa.model.Feedback;
import id.titipmasa.model.Komentar;
import id.titipmasa.model.User;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;

public class FragmentFeedback extends Fragment {

    RecyclerView recyclerView;
    ArrayList<Feedback> listFeedback = new ArrayList<>();
    FirebaseAuth auth;
    DatabaseReference databaseReference;

    FeedbackAdapter adapter;
    String user_id, id_traveller;

    public FragmentFeedback() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_feedback, container, false);

        id_traveller = getActivity().getIntent().getStringExtra("USER_ID");

        user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();

        recyclerView = view.findViewById(R.id.rv_list_feedback);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        displayFeedback();

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        id_traveller = getActivity().getIntent().getStringExtra("USER_ID");
        displayFeedback();
    }

    public void displayFeedback() {

        databaseReference = FirebaseDatabase.getInstance().getReference();
        Query query = databaseReference.child("Feedback").orderByChild("id_traveller").equalTo(id_traveller);
        Log.d("testId", "displayFeedback: "+ id_traveller);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot  dataSnapshot1: dataSnapshot.getChildren()){

                    Feedback feedback = dataSnapshot1.getValue(Feedback.class);

                    listFeedback.add(feedback);
                }

                adapter = new FeedbackAdapter(getContext(), listFeedback);
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("error", "onCancelled: "+ databaseError.getMessage());
            }
        });

    }

}
