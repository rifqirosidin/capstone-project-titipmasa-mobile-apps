package id.titipmasa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import id.titipmasa.model.Komentar;
import id.titipmasa.model.TitipBarang;
import id.titipmasa.model.User;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class DetailRekomendasiBarang extends AppCompatActivity {

    TextView namaBarang, hargaBarang, ketBarang, jumlahBarang, lihatKomentar, buatKomentar, namaTraveller;
    Button request;
    ImageView imgBarang, imgProfilKomentar, imgProfilTraveller;
    FirebaseAuth auth;
    DatabaseReference databaseReference;
    String id_user, user_id;
    String nama_user, imgProfilUser, imgTraveller, id_traveller;
    String nama_traveller;
    String id_barang;
    TextView nama_orang_komentar, isi_komentar;
    String nama, img, isi;


    ProgressDialog progressDialog;
    //getIntent
    String nama_barang, harga_barang, ket_barang, img_barang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.button_pesan_barang);

        namaBarang = findViewById(R.id.nama_barang_rekom);
        hargaBarang = findViewById(R.id.harga_barang_rekom);
        ketBarang = findViewById(R.id.ket_barang_rekom);
        imgBarang = findViewById(R.id.img_barang_rekom);
        imgProfilKomentar = findViewById(R.id.img_profil_komentar);
        imgProfilTraveller = findViewById(R.id.img_profil_traveller);
        request = findViewById(R.id.btn_request);
        lihatKomentar = findViewById(R.id.lihat_komentar);
        buatKomentar = findViewById(R.id.buat_komentar);
        namaTraveller = findViewById(R.id.nama_traveller);
        nama_orang_komentar = findViewById(R.id.nama_orang_komentar);
        isi_komentar = findViewById(R.id.isi_komentar);
        progressDialog = new ProgressDialog(this);

         nama_barang = getIntent().getStringExtra("NAMA_BARANG");
         harga_barang = getIntent().getStringExtra("HARGA_BARANG");
        ket_barang = getIntent().getStringExtra("KET_BARANG");
        img_barang = getIntent().getStringExtra("IMG_BARANG");
         id_barang = getIntent().getStringExtra("ID_BARANG");
        id_traveller = getIntent().getStringExtra("ID_TRAVELLER");
        id_user = FirebaseAuth.getInstance().getCurrentUser().getUid();

        displayUser();
        displayTraveller();
        displayOneKomentar();



        namaBarang.setText(nama_barang);
        hargaBarang.setText(harga_barang);
        ketBarang.setText(ket_barang);


        Picasso.get().load(img_barang).placeholder(R.drawable.ic_image).into(imgBarang);
        Log.d("imgProfil", "onCreate: "+ imgTraveller);




    }

    public void displayUser(){
        databaseReference = FirebaseDatabase.getInstance().getReference("users").child(id_user);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                nama_user = user.getNama();
                imgProfilUser = user.getUrlImageProfil();
                Log.d("nama_user", "pesanBarang: "+ nama_user);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void buatKomentar(View view){

        Intent intent = new Intent(DetailRekomendasiBarang.this, DisplayKomentar.class);
        intent.putExtra("USER_ID", id_user);
        intent.putExtra("ID_BARANG", id_barang);
        startActivity(intent);
    }

    public void displayTraveller(){
        DatabaseReference dfTraveller = FirebaseDatabase.getInstance().getReference("users").child(id_traveller);
        dfTraveller.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User data = dataSnapshot.getValue(User.class);
                nama_traveller = data.getNama();
                imgTraveller = data.getUrlImageProfil();

                namaTraveller.setText(nama_traveller);
                Picasso.get().load(imgTraveller).placeholder(R.drawable.boy).into(imgProfilTraveller);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public void displayOneKomentar(){
        DatabaseReference dfOneKomentar = FirebaseDatabase.getInstance().getReference();
        Query query= dfOneKomentar.child("Komentar").orderByChild("id_barang").equalTo(id_barang).limitToLast(1);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Komentar data = null;
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()) {
                     data = dataSnapshot1.getValue(Komentar.class);

                        String nama = data.getNama();
                        String img = data.getImgProfil();
                        String isi = data.getIsi_komentar();

                        nama_orang_komentar.setText(nama);
                        isi_komentar.setText(isi);

                        Picasso.get().load(img).placeholder(R.drawable.boy).into(imgProfilKomentar);



                }
                if (data == null){
                    nama_orang_komentar.setText("Tidak Ada Komentar");
                    imgProfilKomentar.setVisibility(View.GONE);
                    isi_komentar.setVisibility(View.GONE);
                    lihatKomentar.setVisibility(View.GONE);
                }


                }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("error", "onCancelled: "+ databaseError.getMessage());
            }
        });
    }

    public void pesanBarang(View view){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Anda Yakin");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                progressDialog.setTitle("Proses");
                progressDialog.show();
                DatabaseReference dfPesan = FirebaseDatabase.getInstance().getReference("TitipBarang");
                String idBarang = dfPesan.push().getKey();
                String id_user = FirebaseAuth.getInstance().getCurrentUser().getUid();

                Log.d("nama_user", "pesanBarang: "+ nama_user);
                TitipBarang titipBarang = new TitipBarang(idBarang, id_user, id_traveller, nama_user, nama_barang, "", harga_barang, "", img_barang, "Pending");
                dfPesan.child(idBarang).setValue(titipBarang);

                progressDialog.dismiss();
                dialog.dismiss();
                Toast.makeText(DetailRekomendasiBarang.this, "Sukses", Toast.LENGTH_LONG).show();
            }


        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();


    }
}
