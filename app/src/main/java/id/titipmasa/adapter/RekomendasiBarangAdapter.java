package id.titipmasa.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.DetailRekomendasiBarang;
import id.titipmasa.R;
import id.titipmasa.model.RekomendasiBarang;

public class RekomendasiBarangAdapter extends RecyclerView.Adapter<RekomendasiBarangAdapter.ViewHolderRekomendasiBarang> {

    Context context;
    ArrayList<RekomendasiBarang> rekomendasiBarangs;

    public RekomendasiBarangAdapter(Context context, ArrayList<RekomendasiBarang> rekomendasiBarangs) {
        this.context = context;
        this.rekomendasiBarangs = rekomendasiBarangs;
    }

    public ArrayList<RekomendasiBarang> getRekomendasiBarangs() {
        return rekomendasiBarangs;
    }

    @NonNull
    @Override
    public ViewHolderRekomendasiBarang onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_barang_rekomendasi, parent, false);

        return new ViewHolderRekomendasiBarang(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderRekomendasiBarang holder, int position) {

        holder.namaBarang.setText(getRekomendasiBarangs().get(position).getNama_barang());
        holder.hargaBarang.setText(getRekomendasiBarangs().get(position).getHarga_barang());

        Picasso.get()
                .load(getRekomendasiBarangs().get(position).getImgBarang())
                .placeholder(R.drawable.ic_image)
                .into(holder.imgBarang);

    }

    @Override
    public int getItemCount() {
        return getRekomendasiBarangs().size();
    }

    public class ViewHolderRekomendasiBarang extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView namaBarang, hargaBarang, keterangan;
        ImageView imgBarang;

        public ViewHolderRekomendasiBarang(@NonNull View itemView) {
            super(itemView);

            namaBarang = itemView.findViewById(R.id.nama_barang_rek);
            hargaBarang = itemView.findViewById(R.id.harga_rekom);
            imgBarang = itemView.findViewById(R.id.imgBarangrek);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), DetailRekomendasiBarang.class);
            intent.putExtra("ID_BARANG", getRekomendasiBarangs().get(getAdapterPosition()).getId());
            intent.putExtra("NAMA_BARANG", namaBarang.getText().toString());
            intent.putExtra("HARGA_BARANG", hargaBarang.getText().toString());
            intent.putExtra("GAMBAR_BARANG", getRekomendasiBarangs().get(getAdapterPosition()).getImgBarang());
            intent.putExtra("KET_BARANG", getRekomendasiBarangs().get(getAdapterPosition()).getKeterangan());
            intent.putExtra("IMG_BARANG", getRekomendasiBarangs().get(getAdapterPosition()).getImgBarang());
            intent.putExtra("ID_TRAVELLER", getRekomendasiBarangs().get(getAdapterPosition()).getId_traveller());
            intent.putExtra("ID_TRAVELLER", getRekomendasiBarangs().get(getAdapterPosition()).getUser_id());

            Log.d("img", "onClick: "+ getRekomendasiBarangs().get(getAdapterPosition()).getImgProfilTraveller());
            v.getContext().startActivity(intent);
        }
    }
}
