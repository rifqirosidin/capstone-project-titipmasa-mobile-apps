package id.titipmasa.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.ProfilTravellerLokal;
import id.titipmasa.R;
import id.titipmasa.model.DataTrip;

public class TravellerAdapter extends RecyclerView.Adapter<TravellerAdapter.ViewHolderTraveller> {
    ArrayList<DataTrip> listData;
    Context context;

    public TravellerAdapter( Context context, ArrayList<DataTrip> listData) {
        this.listData = listData;
        this.context = context;
    }

    public ArrayList<DataTrip> getListData() {
        return listData;
    }


    @NonNull
    @Override
    public ViewHolderTraveller onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_display_traveller_lokal, parent,false);

        return new ViewHolderTraveller(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderTraveller holder, int position) {
        holder.nama.setText(getListData().get(position).getNama());
        holder.tujuan.setText(getListData().get(position).getTujuan() + " - " + getListData().get(position).getLokasi());
        holder.kembali.setText(getListData().get(position).getPulang());
        Picasso.get()
                .load(getListData().get(position).getImgProfil())
                .placeholder(R.drawable.boy)
                .into(holder.imgProfil);
    }

    @Override
    public int getItemCount() {

        return getListData().size();
    }

    public class ViewHolderTraveller extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView nama, tujuan, kembali, lokasi;
        ImageView imgProfil;

        public ViewHolderTraveller(@NonNull View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.nama_traveller);
            tujuan = itemView.findViewById(R.id.tujuan_traveller);
            kembali = itemView.findViewById(R.id.tgl_pulang_traveller);
            imgProfil = itemView.findViewById(R.id.img_profil_traveller);


            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), ProfilTravellerLokal.class);
            intent.putExtra("NAMA_TRAVELLER", nama.getText().toString());
            intent.putExtra("TUJUAN_TRAVELLER", tujuan.getText().toString());
            intent.putExtra("KEMBALI_TRAVELLER", kembali.getText().toString());
            intent.putExtra("ID_TRAVELLER", getListData().get(getAdapterPosition()).getId());
            intent.putExtra("LOKASI_TRAVELLER", getListData().get(getAdapterPosition()).getLokasi());
            intent.putExtra("IMG_PROFIL", getListData().get(getAdapterPosition()).getImgProfil());
            intent.putExtra("USER_ID", getListData().get(getAdapterPosition()).getUser_id());
            Log.d("id_traveller", "onClick: " + getListData().get(getAdapterPosition()).getId());
            v.getContext().startActivity(intent);
        }
    }
}
