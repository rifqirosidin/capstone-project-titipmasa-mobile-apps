package id.titipmasa.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.DetailTitipanMasukActivity;
import id.titipmasa.R;
import id.titipmasa.model.TitipBarang;

public class TitipBarangAdapter extends RecyclerView.Adapter<TitipBarangAdapter.ViewHolderTitipanBarang> {

    Context context;
    ArrayList<TitipBarang> listTitipan;

    public TitipBarangAdapter(Context context, ArrayList<TitipBarang> listTitipan) {
        this.context = context;
        this.listTitipan = listTitipan;
    }


    public ArrayList<TitipBarang> getListTitipan() {
        return listTitipan;
    }


    @NonNull
    @Override
    public ViewHolderTitipanBarang onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_daftar_titipan, parent, false);

        return new ViewHolderTitipanBarang(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderTitipanBarang holder, int position) {

        holder.nama_penitip.setText(getListTitipan().get(position).getNama_penitip());
        holder.nama_barang.setText(getListTitipan().get(position).getNamaBarang());
        holder.harga_titipan.setText(getListTitipan().get(position).getHargaBarang());

        Picasso.get()
                .load(getListTitipan().get(position).getImgUrl())
                .placeholder(R.drawable.sepatu)
                .into(holder.gambar_barang);

    }

    @Override
    public int getItemCount() {
        return getListTitipan().size();
    }

    public class ViewHolderTitipanBarang extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView nama_penitip, harga_titipan, nama_barang;
        ImageView gambar_barang;

        public ViewHolderTitipanBarang(@NonNull View itemView) {

            super(itemView);

            nama_penitip = itemView.findViewById(R.id.tv_nama_penitip);
            harga_titipan = itemView.findViewById(R.id.tv_harga_titipan);
            nama_barang = itemView.findViewById(R.id.tv_nama_barang_titipan);
            gambar_barang = itemView.findViewById(R.id.imgBarang);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(itemView.getContext(), DetailTitipanMasukActivity.class);
            intent.putExtra("ID_TITIPAN", getListTitipan().get(getAdapterPosition()).getId_titipan());
            intent.putExtra("NAMA_BARANG", nama_barang.getText().toString().trim());
            intent.putExtra("HARGA_BARANG", harga_titipan.getText().toString());
            intent.putExtra("NAMA_PENITIP", getListTitipan().get(getAdapterPosition()).getNama_penitip());
            intent.putExtra("GAMBAR_BARANG", getListTitipan().get(getAdapterPosition()).getImgUrl());
            intent.putExtra("ID_TRAVELLER", getListTitipan().get(getAdapterPosition()).getId_traveller());
            intent.putExtra("TUJUAN_PENGIRIMAN", getListTitipan().get(getAdapterPosition()).getTujuanPengiriman());
            intent.putExtra("USER_ID_PENITIP", getListTitipan().get(getAdapterPosition()).getUser_id());
            intent.putExtra("JUMLAH_BARANG", getListTitipan().get(getAdapterPosition()).getJumlahBarang());
            itemView.getContext().startActivity(intent);
        }
    }
}
