package id.titipmasa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.R;
import id.titipmasa.model.Feedback;

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolderFeedback> {
    Context context;
    ArrayList<Feedback> listFeedback;

    public FeedbackAdapter(Context context, ArrayList<Feedback> listFeedback) {
        this.context = context;
        this.listFeedback = listFeedback;
    }
    public ArrayList<Feedback> getListFeedback() {
        return listFeedback;
    }

    @NonNull
    @Override
    public ViewHolderFeedback onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feedback, parent, false);

        return new ViewHolderFeedback(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFeedback holder, int position) {

        holder.tvNamaFeedback.setText(getListFeedback().get(position).getNama());
        holder.tvIsiFeedback.setText(getListFeedback().get(position).getIsi_feedback());

        Picasso.get().load(getListFeedback().get(position).getImgProfil()).placeholder(R.drawable.boy).into(holder.imgProfil);


    }

    @Override
    public int getItemCount() {
        return getListFeedback().size();
    }

    public class ViewHolderFeedback extends RecyclerView.ViewHolder {
        TextView tvNamaFeedback, tvIsiFeedback;
        ImageView imgProfil;

        public ViewHolderFeedback(@NonNull View itemView) {
            super(itemView);

            tvNamaFeedback = itemView.findViewById(R.id.nama_user_feedback);
            tvIsiFeedback = itemView.findViewById(R.id.isi_feedback);
            imgProfil = itemView.findViewById(R.id.img_profil_feedback);

        }
    }
}
