package id.titipmasa.adapter;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.DetailTripSayaActivity;
import id.titipmasa.DisplayRekomendasiBarang;
import id.titipmasa.R;
import id.titipmasa.TripSayaActivity;
import id.titipmasa.model.DataTrip;

public class TripSayaAdapter extends RecyclerView.Adapter<TripSayaAdapter.ViewHolderTripSaya> {

    Context context;
    ArrayList<DataTrip> trips;
    DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;


    public TripSayaAdapter(Context context, ArrayList<DataTrip> trips) {
        this.context = context;
        this.trips = trips;
    }


    public ArrayList<DataTrip> getTrips() {
        return trips;
    }


    @NonNull
    @Override
    public ViewHolderTripSaya onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_trip_saya, parent, false);

        return new ViewHolderTripSaya(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderTripSaya holder, final int position) {

        holder.tujuan_trip.setText(getTrips().get(position).getTujuan());
        holder.lokasi_pulang.setText(getTrips().get(position).getLokasi());
        holder.tgl_pulang.setText(getTrips().get(position).getPulang());

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id_trip = getTrips().get(position).getId();
                String user_id = getTrips().get(position).getUser_id();
                String nama = getTrips().get(position).getNama();
                String tujuan = getTrips().get(position).getTujuan();
                String tglPulang = getTrips().get(position).getPulang();
                String lokasiPulang = getTrips().get(position).getLokasi();
                String img = getTrips().get(position).getImgProfil();


                showUpdateDeleteDialog(id_trip, user_id, nama, tujuan, tglPulang, lokasiPulang, img);
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteTrip(getTrips().get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return getTrips().size();
    }

    public class ViewHolderTripSaya extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tujuan_trip, tgl_pulang, lokasi_pulang;
        Button btnEdit, btnDelete;

        public ViewHolderTripSaya(@NonNull View itemView) {
            super(itemView);

            tujuan_trip = itemView.findViewById(R.id.tv_tujuan_trip);
            tgl_pulang = itemView.findViewById(R.id.tv_tanggal_kembali_trip);
            lokasi_pulang = itemView.findViewById(R.id.lokasi_pulang);
            btnEdit = itemView.findViewById(R.id.btn_edit_trip);
            btnDelete = itemView.findViewById(R.id.btn_delete_trip);


        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(itemView.getContext(), DetailTripSayaActivity.class);
            intent.putExtra("ID_TRIP", getTrips().get(getAdapterPosition()).getId());
            itemView.getContext().startActivity(intent);
        }
    }

    private void showUpdateDeleteDialog(final String idTrip, final String user_id, final String nama, String tujuan_trip, String pulang, String lokasi, final String imgProfil) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.edit_trip, null);
        dialogBuilder.setView(dialogView);


        final Spinner tujuan = (Spinner) dialogView.findViewById(R.id.pilih_tujuan_trip);
        final EditText tgl_pulang = dialogView.findViewById(R.id.tgl_pulang_edit);
        final EditText lokasi_pulang = dialogView.findViewById(R.id.edt_lokasi_pulang_edit);

        Button buttonUpdate = (Button) dialogView.findViewById(R.id.btn_update_trip);
        Button btnDate = dialogView.findViewById(R.id.btn_date_edit);

        final AlertDialog b = dialogBuilder.create();
        b.show();

        tgl_pulang.setText(pulang);
        lokasi_pulang.setText(lokasi);

        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                Calendar newCalendar = Calendar.getInstance();

                datePickerDialog = new DatePickerDialog(dialogView.getContext(), new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        tgl_pulang.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });




        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tujuan_traveller = tujuan.getSelectedItem().toString();
                String pulang = tgl_pulang.getText().toString().trim();
                String lokasi = lokasi_pulang.getText().toString().trim();

                if (!TextUtils.isEmpty(lokasi)) {
                    updateTrip(idTrip, user_id, nama, tujuan_traveller, pulang, lokasi, imgProfil);
                    b.dismiss();
                }
            }
        });


    }

    public void updateTrip(String id, String user_id, String nama, String tujuan, String tgl_pulang, String lokasi, String imgProfil) {

        String id_user = FirebaseAuth.getInstance().getCurrentUser().getUid();

        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("Trips").child(id);

        //updating artist
        DataTrip trip = new DataTrip(id, user_id, nama, tujuan, tgl_pulang, lokasi, imgProfil);
        dR.setValue(trip);
        Toast.makeText(context, "Trip Updated", Toast.LENGTH_LONG).show();


    }

    public void deleteTrip(final String id) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle("Anda Yakin");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                DatabaseReference dR = FirebaseDatabase.getInstance().getReference("Trips").child(id);
                dR.removeValue();
                DatabaseReference tripRef = FirebaseDatabase.getInstance().getReference("Trips").child(id);
                tripRef.removeValue();

                TripSayaActivity reload = new  TripSayaActivity();
                reload.disyplay();


                Toast.makeText(context, "Trips Deleted", Toast.LENGTH_LONG).show();


            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
