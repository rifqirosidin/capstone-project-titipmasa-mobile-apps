package id.titipmasa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.R;
import id.titipmasa.model.Komentar;

public class KomentarAdapter extends RecyclerView.Adapter<KomentarAdapter.ViewHolderKomentar> {

    Context context;
    ArrayList<Komentar> lisKomentar;

    public KomentarAdapter(Context context, ArrayList<Komentar> lisKomentar) {
        this.context = context;
        this.lisKomentar = lisKomentar;
    }

    @NonNull
    @Override
    public ViewHolderKomentar onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_item, parent, false);

        return new ViewHolderKomentar(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderKomentar holder, int position) {

        holder.nama.setText(getLisKomentar().get(position).getNama());
        holder.isi.setText(getLisKomentar().get(position).getIsi_komentar());

        Picasso.get()
                .load(getLisKomentar().get(position).getImgProfil())
                .into(holder.imgProfil);

//        if (getLisKomentar().get(position).getImgPesan() != null){
//            Picasso.get()
//                    .load(getLisKomentar().get(position).getImgPesan())
//                    .into(holder.imgPesan);
//        }

    }

    public ArrayList<Komentar> getLisKomentar() {
        return lisKomentar;
    }

    @Override
    public int getItemCount() {
        return getLisKomentar().size();
    }

    public class ViewHolderKomentar extends RecyclerView.ViewHolder {

        TextView isi, nama;
        ImageView imgProfil, imgPesan;


        public ViewHolderKomentar(@NonNull View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.nama_user);
            isi = itemView.findViewById(R.id.isi_komentar);
            imgProfil = itemView.findViewById(R.id.imgProfil);
            imgPesan = itemView.findViewById(R.id.img_komen);

        }
    }
}
