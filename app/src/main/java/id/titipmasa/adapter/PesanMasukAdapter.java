package id.titipmasa.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.DetailPesan;
import id.titipmasa.R;
import id.titipmasa.model.Pesan;
import id.titipmasa.model.User;


public class PesanMasukAdapter extends RecyclerView.Adapter<PesanMasukAdapter.ViewHolderPesanMasuk> {

    Context context;
    ArrayList<Pesan> listPesan;
    FirebaseAuth auth;
    DatabaseReference df;


    public PesanMasukAdapter(Context context, ArrayList<Pesan> listPesan) {
        this.context = context;
        this.listPesan = listPesan;
    }


    public ArrayList<Pesan> getListPesan() {
        return listPesan;
    }



    @NonNull
    @Override
    public ViewHolderPesanMasuk onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_pesan, parent, false);

        return new ViewHolderPesanMasuk(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderPesanMasuk holder, final int position) {

        String cekFoto = getListPesan().get(position).getImgProfil();

        String id_cek = getListPesan().get(position).getId();
        String id_pesan = getListPesan().get(0).getId();
        String id = getListPesan().get(position).getUser_id();
        String id_to_user = getListPesan().get(position).getTo_user_id();
        String id_user = FirebaseAuth.getInstance().getCurrentUser().getUid();

        Log.d("id_to_user", "onBindViewHolder: "+ id_to_user);


        if (id_user.equalsIgnoreCase(id_to_user)) {
            holder.nama.setText(getListPesan().get(position).getFrom_nama());
            Picasso.get()
                    .load(getListPesan().get(position).getImgProfil())
                    .placeholder(R.drawable.boy)
                    .into(holder.imgProfil);
        } else {
            holder.nama.setText(getListPesan().get(position).getTo_name());
            Picasso.get()
                    .load(getListPesan().get(position).getImgProfilPenerima())
                    .placeholder(R.drawable.boy)
                    .into(holder.imgProfil);
        }

        holder.isiPesan.setText(getListPesan().get(position).getIsi_pesan());

    }



    @Override
    public int getItemCount() {
        return getListPesan().size();
    }

    public class ViewHolderPesanMasuk extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView nama, isiPesan;
        ImageView imgProfil;

        public ViewHolderPesanMasuk(@NonNull View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.tv_user_nama);
            isiPesan = itemView.findViewById(R.id.tv_isi_pesan);
            imgProfil = itemView.findViewById(R.id.img_profil);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(itemView.getContext(), DetailPesan.class);
            intent.putExtra("NAMA", nama.getText().toString());
            intent.putExtra("USER_ID", getListPesan().get(getAdapterPosition()).getTo_user_id());

            intent.putExtra("ID_USER_TO_USER", getListPesan().get(getAdapterPosition()).getId());
            itemView.getContext().startActivity(intent);
        }
    }
}
