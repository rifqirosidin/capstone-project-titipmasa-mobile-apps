package id.titipmasa;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.adapter.KomentarAdapter;
import id.titipmasa.model.Komentar;
import id.titipmasa.model.User;

public class DisplayKomentar extends AppCompatActivity {
    RecyclerView recyclerView;

    EditText edtKomentar;
    Button kirim;
    ImageView add_image;
    DatabaseReference databaseReference;
    StorageReference storageReference;
    FirebaseAuth auth;
    ArrayList<Komentar> listKomentar = new ArrayList<>();
    String nama, imgProfil, user_id;
    StorageReference imgKomentar;
    KomentarAdapter adapter;
    int PICK_IMAGE = 100;
    Uri imgUrl;
    String isiKomentar, id_traveller, id_barang;
    TextView nama_orang_komentar, isi_komentar;
    ImageView imgProfilKomentar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_komentar);


        recyclerView = findViewById(R.id.rv_list_komentar);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        edtKomentar = findViewById(R.id.edt_isi_komentar);
        add_image = findViewById(R.id.addImage);

        kirim = findViewById(R.id.btn_kirim);


        id_barang =  getIntent().getStringExtra("ID_BARANG");
         id_traveller = getIntent().getStringExtra("USER_ID");
        user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();

        dataUser();

        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addKomentar();
                edtKomentar.setText("");
                displayKomentar();
            }
        });

        displayKomentar();

    }

    public void TextKomentar(){
        final DatabaseReference dfKomentar = FirebaseDatabase.getInstance().getReference("Komentar");

        String id_komentar = dfKomentar.push().getKey();
        Komentar komentar = new Komentar(id_komentar, user_id, id_barang, nama, isiKomentar,imgProfil, id_traveller, "" );
        dfKomentar.child(id_komentar).setValue(komentar);
        Toast.makeText(this, "Sukses", Toast.LENGTH_LONG).show();

    }


    public void upload(){
        imgKomentar = storageReference.child(System.currentTimeMillis() + "." + getFileExtension(imgUrl));
        final DatabaseReference dfKomentar = FirebaseDatabase.getInstance().getReference("Komentar");
        storageReference = FirebaseStorage.getInstance().getReference("Komentar");
        imgKomentar.putFile(imgUrl).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                imgKomentar.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        String imgPesan = uri.toString().trim();
                        String id_komentar = dfKomentar.push().getKey();
                        Komentar komentar = new Komentar(id_komentar, user_id, id_barang, nama, "",imgProfil, id_traveller, imgPesan );
                        dfKomentar.child(id_komentar).setValue(komentar);
                        Toast.makeText(DisplayKomentar.this, "Sukses", Toast.LENGTH_LONG).show();

                    }
                });
            }
        });
    }

    public void displayKomentar(){
        DatabaseReference dfKomentar = FirebaseDatabase.getInstance().getReference();

        Query query = dfKomentar.child("Komentar").orderByChild("id_barang").equalTo(id_barang);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listKomentar.clear();
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                    Komentar data = dataSnapshot1.getValue(Komentar.class);
                    Log.d("isi", "onDataChange: "+ data.getIsi_komentar());
                    listKomentar.add(data);

                }
                adapter = new KomentarAdapter(DisplayKomentar.this, listKomentar);
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void dataUser(){
        DatabaseReference dfUser = FirebaseDatabase.getInstance().getReference("users").child(user_id);
        dfUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                nama = user.getNama();
                imgProfil = user.getUrlImageProfil();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void addKomentar(){

        isiKomentar = edtKomentar.getText().toString().trim();

        if (imgKomentar != null){
            upload();
        } else {
            TextKomentar();

        }
        displayKomentar();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data ){
        super.onActivityResult(requestCode, resultCode,data);
        if (resultCode== Activity.RESULT_OK&& requestCode ==  PICK_IMAGE){
            upload();

        }
    }

    private String getFileExtension(Uri uri){
        ContentResolver cr = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));
    }


}
