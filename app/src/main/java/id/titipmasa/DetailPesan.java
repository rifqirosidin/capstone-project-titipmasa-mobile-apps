package id.titipmasa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.adapter.PesanAdapter;
import id.titipmasa.adapter.TravellerAdapter;
import id.titipmasa.model.DataTrip;
import id.titipmasa.model.Pesan;
import id.titipmasa.model.TitipBarang;
import id.titipmasa.model.User;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;

public class DetailPesan extends AppCompatActivity {

    EditText isi_pesan;
    DatabaseReference databaseReference, profil, dfPenerima;
    FirebaseDatabase firebaseDatabase;
    FirebaseAuth mAuth;
    StorageReference storageReference, imageRef;
    PesanAdapter adapter;
    RecyclerView rv_list_message;
    ArrayList<Pesan> pesans;
    String nama_pengirim, user_id;
    ProgressBar progressBar;
    String imgProfil;
    String to_user_id;
    String  namaPenerima, imgProfilPenerima;
    String id_user_to_user;
    Uri imageUri;

    private static final int PICK_IMAGE = 100;
    ImageView imgPesan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pesan);

        progressBar = findViewById(R.id.progressBar);
        isi_pesan = findViewById(R.id.messageEditText);
        rv_list_message = findViewById(R.id.rv_message);
        imgPesan = findViewById(R.id.addMessageImageView);
        rv_list_message.setHasFixedSize(true);
        rv_list_message.setLayoutManager(new LinearLayoutManager(this));
        mAuth = FirebaseAuth.getInstance();



        pesans = new ArrayList<>();
        Button btn_send = findViewById(R.id.sendButton);



        final String to_nama = getIntent().getStringExtra("NAMA");
         to_user_id = getIntent().getStringExtra("USER_ID");
        user_id = mAuth.getCurrentUser().getUid();

        setTitle(to_nama);

        final String id_user = getIntent().getStringExtra("ID_USER");

        databaseReference = FirebaseDatabase.getInstance().getReference("Message");
        storageReference = FirebaseStorage.getInstance().getReference("Message");
        dfPenerima = FirebaseDatabase.getInstance().getReference("users").child(to_user_id);
        progressBar.setVisibility(ProgressBar.GONE);

        id_user_to_user = user_id + to_user_id;

        Log.d("id_user_to_user", "onCreate: "+ id_user_to_user);
        displayUsers();
        displayNamaPenerima();
        loadPesan();

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (imageRef == null){
                   textPesan();
               } else {
                   imgPesan();
               }
                isi_pesan.setText("");
                loadPesan();
            }
        });

    }

    public void loadPesan(){

        DatabaseReference dfPesan = FirebaseDatabase.getInstance().getReference();
        Query query = dfPesan.child("Message").orderByChild("id").equalTo(id_user_to_user);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                pesans.clear();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    Pesan card = dataSnapshot1.getValue(Pesan.class);

                    pesans.add(card);
                }
                adapter = new PesanAdapter(DetailPesan.this, pesans);

                rv_list_message.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                progressBar.setVisibility(ProgressBar.GONE);
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void displayUsers(){
        profil = FirebaseDatabase.getInstance().getReference("users").child(user_id);

        profil.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                nama_pengirim = user.getNama();
                imgProfil = user.getUrlImageProfil();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public void textPesan(){
        final String id_pesan = databaseReference.push().getKey();
        final String pesan = isi_pesan.getText().toString().trim();

        Pesan isi = new Pesan(id_user_to_user, user_id, pesan ,nama_pengirim, namaPenerima, to_user_id, imgProfil, imgProfilPenerima, "");

        databaseReference.child(id_pesan).setValue(isi);
    }

    public void imgPesan(){
       imageRef = storageReference.child(System.currentTimeMillis()+"."+getFileExtension(imageUri));
        final String id_pesan = databaseReference.push().getKey();

        final String pesan = isi_pesan.getText().toString().trim();


        imageRef.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        String imgPesan = uri.toString().trim();
                        Pesan isi = new Pesan(id_user_to_user, user_id, pesan ,nama_pengirim, namaPenerima, to_user_id, imgProfil, imgProfilPenerima, imgPesan);

                        databaseReference.child(id_pesan).setValue(isi);
                    }
                });

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }

    public void displayNamaPenerima(){

        dfPenerima.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
               namaPenerima = user.getNama();
                imgProfilPenerima = user.getUrlImageKTP();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void SelectImage(View view){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data ){
        super.onActivityResult(requestCode, resultCode,data);
        if (resultCode== Activity.RESULT_OK&& requestCode ==  PICK_IMAGE){
            imageUri=data.getData();
            imgPesan();
        }
    }

    private String getFileExtension(Uri uri){
        ContentResolver cr = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));
    }
}
