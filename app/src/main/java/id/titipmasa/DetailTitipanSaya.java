package id.titipmasa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import id.titipmasa.model.Feedback;
import id.titipmasa.model.User;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class DetailTitipanSaya extends AppCompatActivity {

    TextView namaBarang, hargaBarang;
    ImageView imgBarang;
    Button kirim;
    EditText edtFeedback;
    FirebaseAuth auth;
    DatabaseReference databaseReference;
    String id_barang, nama_barang, harga_barang, id_traveller;

    //intansiasi variabel display users
    String imgProfil, namaUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_titipan_saya);

        namaBarang = findViewById(R.id.nama_barang);
        hargaBarang = findViewById(R.id.harga_barang);
        imgBarang = findViewById(R.id.img_barang);
        kirim = findViewById(R.id.btn_feedback);
        edtFeedback = findViewById(R.id.edtFeedback);
        auth = FirebaseAuth.getInstance();
        nama_barang = getIntent().getStringExtra("NAMA_BARANG");
        harga_barang = getIntent().getStringExtra("HARGA_BARANG");
        String img_barang = getIntent().getStringExtra("GAMBAR_BARANG");
        id_barang = getIntent().getStringExtra("ID_TITIPAN");
        id_traveller = getIntent().getStringExtra("ID_TRAVELLER");


        namaBarang.setText(nama_barang);
        hargaBarang.setText(harga_barang);
        Picasso.get().load(img_barang).placeholder(R.drawable.ic_image).into(imgBarang);
        displayUser();
        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFeedback();
            }
        });

    }

    public void addFeedback(){
        displayUser();
        String isi_feedback = edtFeedback.getText().toString().trim();

        if (TextUtils.isEmpty(isi_feedback)){
            edtFeedback.setError("kolom ini harus diisi");

        } else {
            databaseReference = FirebaseDatabase.getInstance().getReference("Feedback");
            String id = databaseReference.push().getKey();
            String user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();

            Feedback feedback = new Feedback(id, user_id, id_barang, id_traveller, namaUser,imgProfil, isi_feedback);

            databaseReference.child(id).setValue(feedback);
            Toast.makeText(this, "Sukses", Toast.LENGTH_LONG).show();
            edtFeedback.setText("");
        }

    }

    public void displayUser(){
        String id_user = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference dfUser = FirebaseDatabase.getInstance().getReference("users").child(id_user);

        dfUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                namaUser = user.getNama();
                imgProfil = user.getUrlImageProfil();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
