package id.titipmasa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import id.titipmasa.model.TitipBarang;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class DetailTitipanMasukActivity extends AppCompatActivity {

        Button btn_terima, btn_tolak;
        String namaBarang, hargaBarang, ketBarang, namaPenitip, imgBarang, jumlah_barang, idTitipan, idTraveller, user_id_penitip, tujuan_pengiriman;
        TextView tvNamaBarang, tvHargabarang, tvKet, tvNamaPenitip, tvTujuan;
        ImageView img_barang;
        String checkStatus = null;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_titipan_masuk);

        setTitle("Detail Titipan");

        tvNamaBarang = findViewById(R.id.nama_barang_titipan_masuk);
        tvHargabarang = findViewById(R.id.harga_barang_titipan_masuk);
        tvNamaPenitip = findViewById(R.id.nama_penitip);
        img_barang = findViewById(R.id.imgBarang);
        tvTujuan = findViewById(R.id.tujuan_pengiriman);
        btn_terima = findViewById(R.id.btnTerima);
        btn_tolak = findViewById(R.id.btnTolak);

        progressDialog = new ProgressDialog(this);

        namaBarang = getIntent().getStringExtra("NAMA_BARANG");
        hargaBarang = getIntent().getStringExtra("HARGA_BARANG");
        imgBarang = getIntent().getStringExtra("GAMBAR_BARANG");
        idTitipan = getIntent().getStringExtra("ID_TITIPAN");
        namaPenitip = getIntent().getStringExtra("NAMA_PENITIP");
        idTraveller = getIntent().getStringExtra("ID_TRAVELLER");
        tujuan_pengiriman = getIntent().getStringExtra("TUJUAN_PENGIRIMAN");
         user_id_penitip = getIntent().getStringExtra("USER_ID_PENITIP");
        jumlah_barang = getIntent().getStringExtra("JUMLAH_BARANG");

        tvTujuan.setText(tujuan_pengiriman);
        tvNamaPenitip.setText(namaPenitip);
        tvHargabarang.setText(hargaBarang);
        tvNamaBarang.setText(namaBarang);

        Picasso.get().load(imgBarang).placeholder(R.drawable.ic_image).into(img_barang);

        checkBarang();


    }

    public void Terima(View view){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Proses");
        progressDialog.show();
        DatabaseReference dfBarang = FirebaseDatabase.getInstance().getReference("TitipBarang").child(idTitipan);

        TitipBarang titipBarang = new TitipBarang(idTitipan, user_id_penitip, idTraveller, namaPenitip, namaBarang,jumlah_barang, hargaBarang, tujuan_pengiriman, imgBarang, "Diterima");
        dfBarang.setValue(titipBarang);

        progressDialog.dismiss();
        Toast.makeText(this, "Sukses Barang Diterima", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(DetailTitipanMasukActivity.this, TitipanMasukActivity.class);
        startActivity(intent);
        finish();
    }

    public void Tolak(View view){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Proses");
        progressDialog.show();
        DatabaseReference dfBarang = FirebaseDatabase.getInstance().getReference("TitipBarang").child(idTitipan);

        TitipBarang titipBarang = new TitipBarang(idTitipan, user_id_penitip, idTraveller, namaPenitip, namaBarang,jumlah_barang, hargaBarang, tujuan_pengiriman, imgBarang, "Ditolak");
        dfBarang.setValue(titipBarang);

        progressDialog.dismiss();
        Toast.makeText(this, "Sukses Barang ditolak", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(DetailTitipanMasukActivity.this, TitipanMasukActivity.class);
        startActivity(intent);
        finish();
    }

    public void checkBarang(){
        final DatabaseReference check = FirebaseDatabase.getInstance().getReference("TitipBarang").child(idTitipan);

        check.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                TitipBarang titipBarang = dataSnapshot.getValue(TitipBarang.class);
                checkStatus = titipBarang.getStatus();

                if (checkStatus.equalsIgnoreCase("Diterima") || checkStatus.equalsIgnoreCase("Ditolak")){
                    btn_terima.setVisibility(View.GONE);
                    btn_tolak.setVisibility(View.GONE);
                } else {
                    btn_terima.setVisibility(View.VISIBLE);
                    btn_tolak.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
